### Pleroma Utilities

Collection of shell (bash) scripts to help manage pleroma instance for admins.

1. backup - scripts for automating backup of pleroma instance.

**LICENSE**

GNU AGPL v3

### backup

There are two bash scripts, one for server where the pleroma instance is running and another where the backup has to be copied.

1. Copy the `pleroma_backup_server.sh` to the server and modify the script to reflect the path of pleroma folder.
2. Add a `crontab` and let it do regular backup of files, folders and compress them as a tar.7z archive (`p7zip-full`) package is required.
3. Copy the `pleroma_backup_client.sh` to the machine where you want the backups to be downloaded.
4. Make sure this machine can `ssh` into the server machine.
5. Modify the client script to reflect the ssh credentials and path of the folders.
6. Add a `crontab` and let it automatically download the backups for you.

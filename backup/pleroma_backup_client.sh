#!/bin/bash

#
#  Copyright 2019 Prasanna Venkadesh
#  Author: Prasanna Venkadesh
#  License: GNU AGPL v3
#

set -e

# path of the backup file in remote server
REMOTE_LOC="user@host:/path/to/backup.tar.7z"

# local path where to copy the backup file in local computer
BACKUP_LOC="$HOME/pleroma_backup"

# name of the folder with timestamp in local computer
LOCAL_FOLDER_NAME="pleroma-"$(date '+%d-%b-%Y:%H:%M:%S')

# create local backup folder
mkdir $BACKUP_LOC/$LOCAL_FOLDER_NAME

# copy from server to local folder
echo "Downloading from Server ..."
scp $REMOTE_LOC $BACKUP_LOC/$LOCAL_FOLDER_NAME

echo "============================"
echo "DONE"

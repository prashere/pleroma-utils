#!/bin/bash

#
#  Copyright 2019 Prasanna Venkadesh
#  Author: Prasanna Venkadesh
#  License: GNU AGPL v3
#

# clear previous backup, if any
rm -rf $HOME/backup.tar.7z

BACKUP_FOLDER="$HOME/backup"
PLEROMA_LOC="$HOME/pleroma"

# if the backup folder doesn't exist, then create
if [ ! -d $BACKUP_FOLDER ]; then
        mkdir $BACKUP_FOLDER
fi

TZ=Asia/Kolkata date > $BACKUP_FOLDER/timestamp

# database dump, uploads folder, generic and production configurations
pg_dump -d pleroma_dev --format=custom -f $BACKUP_FOLDER/pleroma.pgdump
cp -r $PLEROMA_LOC/uploads $BACKUP_FOLDER/
cp $PLEROMA_LOC/config/config.exs $PLEROMA_LOC/config/prod.secret.exs $BACKUP_FOLDER/

# compress the folder and remove it
tar -C $BACKUP_FOLDER -cf - . | 7z a -si $BACKUP_FOLDER.tar.7z
rm -rf $BACKUP_FOLDER
